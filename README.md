# 1_NSI_22

Dépôt de fichiers de __première NSI__ Lycée M de Flandre à Gondecourt  
C. Ghesquiere : cecile.ghesquiere@ac-lille.fr  
lien vers le réseau du lycée : [ici](https://194.167.100.29:8443/owncloud/index.php) 

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-010.gif)  

__chapitre 20 : Algorithme KNN__  
* [Cours](./Chapitres/c20_knn/1_Cours/cours_algo_knn.md)  
* [TP](https://framagit.org/CecGhesq/1_nsi_22/-/blob/main/Chapitres/c20_knn/2_Activit%C3%A9s_TP_Jupyter/TP_knn.ipynb)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-001.gif)  

__chapitre 19 : Algorithme glouton__  
* [Cours](./Chapitres/c19_glouton/cours_algo_glouton.md)  
* [TP](./Chapitres/c19_glouton/TP_haie_fleurie.md)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-002.gif)  

__chapitre 18 : Recherche dichotomique__  
* [Cours](./Chapitres/c18_dichotomie/cours/cours_dichotomie.md)  
* [TP](./Chapitres/c18_dichotomie/tp_dichotomie/tp_dichotomie.md)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-003.gif)  
__chapitre 17 : Tris par sélection et insertion des listes__ 

* [cours à compléter](./Chapitres/c17_tris/C17 LES TRIS.ipynb)  

* [TP](./Chapitres/c17_tris/TP_tris.md)  


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-004.gif)  
__chapitre 16 : Réseau__  
* [Cours HTTP](./Chapitres/c16_reseau/c16_cours_http.md)  
    * [exercices](./Chapitres/c16_reseau/exercices/c16_exos_client_serveur.md) 
    * [Utilisations de formulaires](./Chapitres/c16_reseau/C16_1_formulaire.md) 
* [cours TCP](./Chapitres/c16_reseau/c16_2_cours_tcp.md)
    * [tp1 Filius : table ARP](./Chapitres/c16_reseau/tp1_filius/tp1_filius.md)
    * [tp2 : TCP](./Chapitres/c16_reseau/tp2_tcp/tp2_tcp.md)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-005.gif)  

__chapitre 15 : interactions homme machine__
* Découverte du [javascript](./Chapitres/c15_ihm/c15_p1_decouvertejs/c15_decouverte_js.md) 
    + [appli2](./Chapitres/c15_ihm/c15_p1_decouvertejs/appli2.js)  
    + [appli3](./Chapitres/c15_ihm/c15_p1_decouvertejs/appli3.js)  
    + [appli4](./Chapitres/c15_ihm/c15_p1_decouvertejs/appli4.js)  
* Programmation évenementielle  
    + [cours](./Chapitres/c15_ihm/c15_p2_evenementiel/c15_evenementiel.md)  
    + [TP](./Chapitres/c15_ihm/c15_p2_evenementiel/tp_evenements/C15_2_tp_evenements.md)  

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-006.gif)  
 
 __chapitre 14 : les dictionnaires__  
* Cours : [Généralités](./Chapitres/c14_dictionnaire/C14_cours/c14_dictionnaire.md)  
* [Compléments](./Chapitres/c14_dictionnaire/C14_cours/C14_dictionnaire_compléments.md) du cours  
* [Exercices](./Chapitres/c14_dictionnaire/exos/C14_exercices.md)  
* Où se repose Spiderman entre deux exploits ? [ici](./Chapitres/c14_dictionnaire/C14_TP_Données EXif_dico.md)


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-007.gif)  

__chapitre 13 : les tuples__  

* Cours [en ligne](./Chapitres/c13_tuples/C12_tuples.md) à travailler pour le 2/2 ou  [![](./img/pdf.jpg)](./Chapitres/c13_tuples/C12_tuples.pdf)  
* TP [en ligne](./Chapitres/c13_tuples/c12_tp.md) ou [![](./img/pdf.jpg)](./Chapitres/c13_tuples/C12_TP_tuples.pdf) 
![](./img/53630_r.gif)  -->__correction TD tuples première partie__ : [ici](/Chapitres/c13_tuples/TD_tuples_partie1.md)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif)  

__Chapitre 12 : Mise au point des programmes__  

* Cours [en ligne](./Chapitres/c12_mise_au_point/C12_mise_au_point.md)  ou [![](./img/pdf.jpg)](./Chapitres/c12_mise_au_point/C12_mise_au_point.pdf) 
* TP [en ligne](./Chapitres/c12_mise_au_point/C12_TP_mise_au_point_programmes.md)  


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif)  

__Chapitre 11 : Système d'exploitation__ 

* [activité introductive](./Chapitres/c11_systeme_exploitation/ac_intro/C11_os_intro.md) ou [pdf](./Chapitres/c11_systeme_exploitation/ac_intro/C11_os_intro.pdf)  
* cours 11_1 : [Systèmes d'exploitation](./Chapitres/c11_systeme_exploitation/cours/C11_1_os.md)  ou  [![](./img/pdf.jpg)](.Chapitres/c11_systeme_exploitation/cours/C11_1_os.pdf)  
* [TP Shell](./Chapitres/c11_systeme_exploitation/tp_shell/tp_shell_weblinux.md) ou   [![](./img/pdf.jpg)](./Chapitres/c11_systeme_exploitation/tp_shell/tp_shell_weblinux.pdf)  
* [TP Terminus](./Chapitres/c11_systeme_exploitation/tp_shell/TD_Jeu_Terminus.pdf)  A FAIRE EN AUTONOMIE
* Cours 11_2 : [droits des utilisateurs](./Chapitres/c11_systeme_exploitation/cours/C11_2_droits_utilisateurs.md) ou [![](./img/pdf.jpg)](./Chapitres/c11_systeme_exploitation/cours/C11_2_droits_utilisateurs.pdf)  
* TP Droits : [sujet en ligne](./Chapitres/c11_systeme_exploitation/tp_droits/C11_2_TP_droits.md) avec le fichier à télécharger [ici](./Chapitres/c11_systeme_exploitation/tp_droits/frozen-bubble-2_2_0.zip)  

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-010.gif) 
 
__Chapitre 10 : HTML / CSS__ :  
Vous travaillerez en autonomie sur ce chapitre dont le but sera de réaliser un site Web de votre visite au forum  : qu'avez vous appris? apprécié ? quelle(s) découverte(s) avez-vous faites ? quel est le lien hypertexte vers une école, une entreprise que vous conseillez ?  
Des photos prises lors du forum sont présentes dans ce [dossier](./Chapitres/c10_html/forum_numerique)  


Pour vous aidez à créer votre site WEB, vous pouvez suivre :

* le parcours [débutant](./Chapitres/c10_html/html_css_niveau_debutant/HTML5_CSS3.pdf) ; dans ce [dossier](./Chapitres/c10_html/html_css_niveau_debutant/) les fichiers nécessaires.  
--> liens vers des sites sur flexbox :
    *  https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox  
    * https://www.alsacreations.com/tuto/lire/1493-css3-flexbox-layout-module.html
    * https://www.pccindow.com/fr/flexbox-css/


* le parcours [avancé](./Chapitres/c10_html/html_css_niveau_avance/Projet_HTML_CSS.pdf) permettant de faire de jolies transitions à l'aide de boites , dans ce [dossier](./Chapitres/c10_html/html_css_niveau_avance/) les fichiers nécessaires.  
    --> Transitions  
https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Transitions/Utiliser_transitions_CSS

    --> Site généraliste en CSS3  
http://www.css3create.com/  

![](./img/noel_de64.gif)  ![](./img/noel_sa02.gif)  



![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-001.gif)  

__Chapitre 9 : les tableaux : type list en PYTHON__ 

* Le type list : cours 1 [en ligne](./Chapitres/c9_listes/C9 cours1/cours/C9_1_liste_tab.md) 
* TP_1 : [en ligne](./Chapitres/c9_listes/C9 cours1/TP/C9_1_TP_liste.md)    
* travail en autonomie sur [e-nsi](https://e-nsi.forge.aeif.fr/pratique/A/10-tab_recherche/)  
* Listes en compréhension ; listes 2 D [en ligne](./Chapitres/c9_listes/C9 cours2 listes en Python/cours/C9_2_listes_en_comprehension.md)  ou [pdf](./Chapitres/c9_listes/C9 cours2 listes en Python/cours/C9_2_listes_en_comprehension.pdf)  
* TP_2 : [en ligne](./Chapitres/c9_listes/C9 cours2 listes en Python/TP/C9_2_TP_liste_en_comp.md)  ou [pdf](./Chapitres/c9_listes/C9 cours2 listes en Python/TP/C9_2_TP_liste_en_comp.pdf)  
    
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-002.gif)  

__Chapitre 8 : Parcours séquentiel dans une chaîne__  
    * cours : cours [en ligne](./Chapitres/c8_parcours_sequentiel/c8_for_string.md) ou [![](./img/pdf.jpg)](Chapitres/c8_parcours_sequentiel/c8_for_string.pdf)  
    * TP [en ligne](./Chapitres/c8_parcours_sequentiel/tp_parcours_chaines.md) ou [![](./img/pdf.jpg)](Chapitres/c8_parcours_sequentiel/tp_parcours_chaines.pdf)  

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-003.gif)  

__Chapitre 7 : Architecture matérielle__  
    * Architecture d'un ordinateur : cours [en ligne](./Chapitres/c7_architecture_materielle/C7_1/C7_1_Archi_ordi.md) ou [![](./img/pdf.jpg)](./Chapitres/c7_architecture_materielle/C7_1/C7_1.pdf)  
    * Algèbre de Boole et circuits combinatoires: cours [en ligne](./Chapitres/c7_architecture_materielle/C7_2/C7_2.md) ou [![](./img/pdf.jpg)](./Chapitres/c7_architecture_materielle/C7_2/C7_2.pdf)   
    * TP1 logique [en ligne](./Chapitres/c7_architecture_materielle/C7_2/TP_Portes_logiques_CircuitVerse_eleves.md) ou [![](./img/pdf.jpg)](./Chapitres/c7_architecture_materielle/C7_2/TP1_CircuitVerse.pdf)  
    * TP2 circuits combinatoires [en ligne](./Chapitres/c7_architecture_materielle/C7_2/TP2_circuits_combinatoires_eleves.md) ou [![](./img/pdf.jpg)](./Chapitres/c7_architecture_materielle/C7_2/TP2.pdf) --> [correction](./Chapitres/c7_architecture_materielle/C7_2/circuits_combinatoires_tp2.pdf)  
    * Exercices d'application : [en ligne](./Chapitres/c7_architecture_materielle/C7_2/Exercices_C7_1_2.md)  
    * Le langage machine : cours [en ligne](./Chapitres/c7_architecture_materielle/C7_3/7_3_cours.md)  
    * TP3 utilisation de la M999: télécharger le [dossier zippé](./Chapitres/c7_architecture_materielle/C7_3/M999.zip) de la machine puis dézipper-le. Sujet d'activité [en ligne](./Chapitres/c7_architecture_materielle/C7_3/C7_3_TP_langage_assembleur.md)   

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-004.gif)  

__Chapitre 6 : la boucle bornée FOR__  
    * cours : cours [en ligne](./Chapitres/c6_boucle_bornee/C6_cours_for.md) ou [![](./img/pdf.jpg)](c6_boucle_bornee/C6_cours_for.pdf) ou [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2FCecGhesq%2F1_nsi_21/HEAD?labpath=Chapitres%2Fc6_boucle_bornee%2Fnb_for%2Fboucles_for.ipynb)   
    * TP [en ligne](./Chapitres/c6_boucle_bornee/tp_for/tp_for.md) ou [![](./img/pdf.jpg)](./Chapitres/c6_boucle_bornee/tp_for/tp_for.pdf)  

---
__DS 2__ :_partie pratique_ chapitres 4 et 5 [sujet_A](./Chapitres/DS/DS2/DS2_A.md) , [sujet A corrigé](./Chapitres/DS/DS2/DS2_A.py) , [sujet_B](./Chapitres/DS/DS2/DS2_B.md) , [sujet B corrigé](./Chapitres/DS/DS2/DS2_B.py) 

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-005.gif)  

__Chapitre 5 : les booléens; conditions__ 

* cours : cours [en ligne](./Chapitres/c5_booléen_conditions/cours_booleens_conditions.md) ou [![](./img/pdf.jpg)](./Chapitres/c5_booléen_conditions/cours_booleens_conditions.pdf)  
* TP [en ligne](./Chapitres/c5_booléen_conditions/tp_booleens_conditions/tp_booleens_conditions.md) ou [![](./img/pdf.jpg)](./Chapitres/c5_booléen_conditions/tp_booleens_conditions/tp_booleens_conditions.pdf)  
---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-006.gif)  

__Chapitre 4 : La boucle non bornée TANT QUE__  
    * cours : cours [en ligne](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Chapitres/c4_while/c4_while_algo_2020_git.md) ou [![](./img/pdf.jpg)](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Chapitres/c4_while/C4_while_algo_2020.pdf)  
    * TP [en ligne](./Chapitres/c4_while/tp/tp_boucles_while.md) ou [![](./img/pdf.jpg)](./Chapitres/c4_while/tp/tp_boucles_while_v1.pdf)  

---

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-007.gif)  
__Chapitre 3 : LES ENTIERS RELATIFS__  
 * cours :  [en ligne](./Chapitres/c3_entiers_relatifs/C3_complementadeux_git.md)
 * TP [en ligne](./Chapitres/c3_entiers_relatifs/C3_exercices_complement_a_deux.md)

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif)  

__Chapitre 2 : LES ENTIERS__

* cours [ en ligne](./Chapitres/c2_representation_entiers/C2_les_bases.md) ou [![](./img/pdf.jpg)](./Chapitres/c2_representation_entiers/C2_les_bases.pdf)
* TP [en ligne](./Chapitres/c2_representation_entiers/TP/tp1_bases.md) ou [![](./img/pdf.jpg)](./Chapitres/c2_representation_entiers/TP/tp1_bases.pdf)

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif)  

__Chapitre 1 : DECOUVERTE DU PYTHON__  
* C1_1 Manipuler des nombres
    * cours [ en ligne](./Chapitres/c1_decouverte_python/manipuler_nombres/C1_1.md) ou [![](./img/pdf.jpg)](./Chapitres/c1_decouverte_python/manipuler_nombres/C1_1.pdf)
    * TP [en ligne](./Chapitres/c1_decouverte_python/manipuler_nombres/tp1_nombres.md) ou [![](./img/pdf.jpg)](./Chapitres/c1_decouverte_python/manipuler_nombres/tp1_nombres.pdf)


* C1_2 Les fonctions :
    *   [cours](./Chapitres/c1_decouverte_python/python2_fonctions/cours2_fonctions.md) ou [![](./img/pdf.jpg)](./Chapitres/c1_decouverte_python/python2_fonctions/cours2_fonctions.pdf)
    * TP  [en ligne](./Chapitres/c1_decouverte_python/python2_fonctions/tp2_fonctions/tp2_fonctions.md)  
[lien vers exercices corrigés du TP](https://framagit.org/CecGhesq/1_nsi_22/-/tree/main/Chapitres/c1_decouverte_python)
* C1_3 Le type chaine :
    * [cours](./Chapitres/c1_decouverte_python/python3_strings/cours3_strings.md) ou [![](./img/pdf.jpg)](/Chapitres/c1_decouverte_python/python3_strings/pdf/cours3_strings.pdf)
    * TP [en ligne](./Chapitres/c1_decouverte_python/python3_strings/tp3_strings/tp3_strings.md)
---

=======
![](./img/cheminee-013.gif) __Site web__ à déposer sur Pronote pour le __mardi 3 janvier__ au plus tard  ![](./img/cheminee-013.gif)  
---
![](./img/53630_r.gif) Projets à rendre sur Pronote au plus tard le 7 novembre :  
* [le permis de conduire](./Projets/permis_a_point.md)  
* [la plus jolie figure sur votre calculatrice](./Projets/projet_turtle.md)  



Lien vers le site relatant la sortie au forum d'Orchies : [ici](https://nsi-margueritedeflandre.etab.ac-lille.fr/)

Notice pour __apprendre à zipper (compresser) un dossier__ : [ici](./Notices/Notice_Zip.pdf)
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> C GHESQUIERE</a><br />Les documents sont mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.

