<center> <h1> Gestion du permis à points </h1> </center>

Vous allez devoir créer une application de gestion du permis à points.  
__LIRE__ [ceci](https://www.service-public.fr/particuliers/vosdroits/F31551) pour connaître les infractions liées aux excès de vitesse.



1. Créer sous forme de chaines de caractères les infractions liées au non respect des limitations de vitesse associés à des codes nommés v1 ,...  v7.

```python
v1 = 'Excès de vitesse inférieur à 20 km/h'
```

2. Ecrire une fonction `affichage_exces_vitesse()` qui affiche les différentes infractions avec leur code (v1, v2, ....).

```python
>>> affichage_exces_vitesse()
v1 Excès de vitesse inférieur à 20 km/h
v2 Excès de vitesse ≤ 20 km/h si vitesse maximale autorisée ≤ 50 km/h
v3 Excès de vitesse ⩾ à 20 km/h et inférieur à 30 km/h
v4 Excès de vitesse ⩾ à 30 km/h et inférieur à 40 km/h
v5 Excès de vitesse ⩾ à 40 km/h et inférieur à 50 km/h
v6 Excès de vitesse supérieur à 50 km/h
v7 Transport, détention, usage d'appareil destiné à déceler ou perturber les contrôles de vitesse (détecteurs de radars)
```

3. Ecrire une fonction  `calcul_solde(nb)` ayant pour argument __nb__ nombre de points possédés par le permis du conducteur avant l'infraction. La fonction devra tout d'abord afficher toutes les infractions avec leur code , demander à l’utilisateur de saisir le code de l’infraction commise (v1, v2, etc ),  retirer le nombre de points correspondant , __renvoyer__ le solde de points. __Tant que la réponse n'est pas un code valide, la question est reposée__.

```python
>>> infraction(12)
v1 Excès de vitesse inférieur à 20 km/h
v2 Excès de vitesse ≤ 20 km/h si vitesse maximale autorisée ≤ 50 km/h
v3 Excès de vitesse ⩾ à 20 km/h et inférieur à 30 km/h
v4 Excès de vitesse ⩾ à 30 km/h et inférieur à 40 km/h
v5 Excès de vitesse ⩾ à 40 km/h et inférieur à 50 km/h
v6 Excès de vitesse supérieur à 50 km/h
v7 Transport, détention, usage d'appareil destiné à déceler ou perturber les contrôles de vitesse (détecteurs de radars)
Quel est le code d' infraction que avez vous commise ? v2
11

>>> calcul_solde(12)
v1 : Excès de vitesse inférieur à 20 km/h
v2 : Excès de vitesse ≤ 20 km/h si vitesse maximale autorisée ≤ 50 km/h
v3 : Excès de vitesse ⩾ à 20 km/h et inférieur à 30 km/h
v4 : Excès de vitesse ⩾ à 30 km/h et inférieur à 40 km/h
v5 : Excès de vitesse ⩾ à 40 km/h et inférieur à 50 km/h
v6 :  Excès de vitesse supérieur à 50 km/h
v7 :  Transport, détention, usage d'appareil destiné à déceler ou perturber les contrôles de vitesse (détecteurs de radars)
Quel est le code d' infraction que avez vous commise ? cd
Quel est le code d' infraction que avez vous commise ? sg
Quel est le code d' infraction que avez vous commise ? v4
9
```

4. Ecrire le prédicat vérifiant que le permis est valide ( le nombre de points est positif )

```python
>>> est_valide(6)
True
>>> est_valide(0)
False
```


5. Réaliser la fonction générale `points_permis()` : 
* qui demande le nom de la personne ainsi que son solde de points avant l'infraction
*  qui calcule son nouveau solde de point qui sera stocké dans une variable
* qui affiche le nombre de points restants pour la personne ou la phrase 'Le solde du permis de conduire de XXX est nul'.

On utilisera absolument les fonctions écrites précédemment.

```python
>>> points_permis()
Quel est votre nom ? MIRTE
Combien de points disposiez-vous avant l'infraction ? 4
v1 : Excès de vitesse inférieur à 20 km/h
v2 : Excès de vitesse ≤ 20 km/h si vitesse maximale autorisée ≤ 50 km/h
v3 : Excès de vitesse ⩾ à 20 km/h et inférieur à 30 km/h
v4 : Excès de vitesse ⩾ à 30 km/h et inférieur à 40 km/h
v5 : Excès de vitesse ⩾ à 40 km/h et inférieur à 50 km/h
v6 :  Excès de vitesse supérieur à 50 km/h
v7 :  Transport, détention, usage d'appareil destiné à déceler ou perturber les contrôles de vitesse (détecteurs de radars)
Quel est le code d' infraction que avez vous commise ? v4
MIRTE dispose de  1 points sur son permis de conduire

>>> points_permis()
Quel est votre nom ? MIRTE
Combien de points disposiez-vous avant l'infraction ? 4
v1 : Excès de vitesse inférieur à 20 km/h
v2 : Excès de vitesse ≤ 20 km/h si vitesse maximale autorisée ≤ 50 km/h
v3 : Excès de vitesse ⩾ à 20 km/h et inférieur à 30 km/h
v4 : Excès de vitesse ⩾ à 30 km/h et inférieur à 40 km/h
v5 : Excès de vitesse ⩾ à 40 km/h et inférieur à 50 km/h
v6 :  Excès de vitesse supérieur à 50 km/h
v7 :  Transport, détention, usage d'appareil destiné à déceler ou perturber les contrôles de vitesse (détecteurs de radars)
Quel est le code d' infraction que avez vous commise ? v6
Le solde du permis de conduire de MIRTE est nul
```

Déposer votre fichier python sur Pronote.
