---
title: "Chapitre 13 : les tuples"  
subtitle: "TD corrigé"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---


# Exercice 1

Soit le tuple (4,8,'trou', 'gagné', 7, True) écrit en Python :

1. Comment demander pour que la valeur 'gagné' soit renvoyée?
2. Comment demander pour obtenir le nouveau tuple :('trou', True)
3. Comment demander la première valeur de ce nouveau tuple?
4. Que donnera tuple[:5] ?
5. Que donnera tuple[5:] ?
6. Que donnera tuple[1:4:2] ?

```python
tuple=(4,8,'trou','gagné',7, True)
```

```python
 tuple[2]
```

```python
    'trou'
```

```python
nveau_tuple=tuple[2] , tuple[5]
nveau_tuple[0]
```

```python
    'trou'
```

```python
tuple[:5]
```

```python
    (4, 8, 'trou', 'gagné', 7)
```

```python
 tuple[5:]
```

```python
    (True,)
```

```python
tuple[1:4:2]
```

```python
    (8, 'gagné')
```

# Exercice 2

On souhaite définir une fonction *is_square(t)* admettant un triplet t pour argument, ce triplet contenant la longueur des trois côtés d'un triangle.
Compléter le programme suivant afin qu'il vérifie si le triangle est rectangle ou non.

```python
def is_square(t):
    if  .... or .......  or ...... :
        print("ce triangle est rectangle")
    else : 
        print("ce triangle n'est pas rectangle")
```

```python
def is_square(t):
    if (t[0]**2 + t[1]**2 ==t[2]**2) or (t[0]**2 + t[2]**2 == t[1]**2) or (t[2]**2 + t[1]**2 == t[0]**2):
        print("ce triangle est rectangle")
    else : 
        print("ce triangle n'est pas rectangle")
```

```python
tuple=(3,4,5)
is_square(tuple)
```

```python
    ce triangle est rectangle
```

# Exercice 3

Créer un tuple avec par exemple une liste de prénoms.

1. Créer une fonction permettant de savoir si une personne est dans cette liste en utilisant une boucle `for`. La fonction doit répondre si la personne est présente ou absente.


```python
groupe=('Akim',' Nadia', 'Elise','Virginie','Guillaume', 'Louis', 'Julie', 'Naïma', 'Mohamed', 'Maria', 'Zélie', 'Antoine', 'Bryan', 'Milo', 'Fatou', 'Oscar', 'Julien', 'Cyril')
def est_present(prenom):
    absent= True # permet de renvoyer si le prénom est absent de la liste ; booleen
    for var in groupe :
        if prenom == var:
            absent =False
            print (prenom, "est présent.e")
    if absent == True :
        print (prenom,"est absent.e")
```

```python
est_present('Elise')
```

```python
    Elise est présent
```

2. Refaire une version de la fonction en utilisant `ìn` : c'est bien plus rapide!!!!

```python
def est_present2(prenom):
    if prenom in groupe :
        print (prenom,"est présent.e")
    else : 
        print(prenom,"est absent.e")
```

```python
est_present2('Eli')
```

```python
    Eli est absent.e
```
