# Comprendre quelques fonctions du module turtle

[documentation 1](https://docs.python.org/fr/3/library/turtle.html)

[documentation 2](https://www.cours-gratuit.com/tutoriel-python/tutoriel-python-matriser-le-module-turtle)  

a) A l’aide du lien vers la documentation fourni dans le support de cours, indiquez à quoi sert la fonction forward() et quel(s) paramètre(s) doivent être utilisé(s) .  

b) Même question pour la fonction left()  

c) Même question pour la fonction color()  

# Dessiner avec Python  

__appli 1__ :  
Écrire un script qui dessine un carré de côté 50 pixels, en trait noir.  

![](carre.png)

__appli 2__ :  
Écrire un script qui dessine un carré de côté 50 pixels, en trait rouge et noir alternativement.
 
__appli 3__ :  
Ecrire un script demandant à l’utilisateur d’entrer la couleur du stylo à utiliser puis qui dessine un triangle de côté 80 pixels. 

__appli 4__ :  
Ecrire une fonction `double_carre(c)` permettant de tracer un carré avec turtle si on lui fournit en paramètre _c_ la longueur d’un côté. Demander à l’utilisateur d’entrer une valeur pour c puis dessiner 2 carrés en appelant cette fonction et en décalant votre stylo de deux fois la largeur du carré entre chaque dessin.  

![](carre_2.png)  

__appli 5__ :  
• Définir une fonction `triangle(t,colort)` dessinant un triangle équilatéral de côté _t_ et couleur _colort_  
• Définir une fonction `carre(c,colorc)` dessinant un carré, de côté _c_ et de couleur _colorc_  
• Définir une fonction `hexagone(h,colorh)` dessinant un hexagone de côté _h_ et de couleur _colorh_  
• Demander à l’utilisateur de fournir des valeurs pour _t,c, h, colort, colorc_ et _colorh_ puis dessiner les formes. Les côtés horizontaux de chaque figure devront être distants de 100px.  Enregistrer le script sous le nom  `figure_turtle.py`

```python
>>> %Run figure_turtle.py
Quelle est la longueur du côté du carré souhaité?40
Quelle couleur doit être ce carré?red
Quelle est la longueur du côté du triangle souhaité?47
Quelle couleur doit être ce carré?blue
Quelle est la longueur du côté de l'hexagone souhaité?50
Quelle couleur doit être cet hexagone?green
```

![](color.png)   


__appli 6__ Créez un programme demandant à l’utilisateur quelle figure géométrique il souhaite tracer parmi les suivantes : rectangle, carré, hexagone, triangle equilateral.
Vous définirez une fonction pour chacun des cas.
Dans chacune de ces fonctions demandez la couleur du stylo à utiliser ainsi que la longueur des côtés.
Enregistrez votre script sous le nom `trace_figures.py`


__appli 7__ [  Pour aller plus loin   ](https://framagit.org/CecGhesq/1_nsi_22/-/blob/main/Projets/projet_turtle.md)
