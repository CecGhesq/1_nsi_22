######### SUJET A ################################
## EXERCICE 1     5 pts

# Ecrire les prédicats suivants (réponse booléenne True ou False)  :
def est_negatif(nombre):
    '''renvoie si le nombre est stricitement négatif
    '''
    pass

#print('-4 est négatif : ', est_negatif(-4))

def est_impair(nombre):
    ''' Renvoie si le nombre est impair
    '''
    pass

#print('23 est un nombre impair: ',est_impair(23))    # appel à décommenter pour tester

def est_multiple_5(nombre):
    '''renvoie si le nombre est multiple de 5
    '''
    pass
# print('25 est un multiple de 5 : ', est_multiple_5(25)) # appel à décommenter pour tester

def est_dans_la_gamme(valeur):
    '''renvoie si la valeur est comprise entre 400 et 800 (valeurs incluses)
    '''
    pass

#print('345 est dans la gamme : ',est_dans_la_gamme(345))   # appel à décommenter pour tester

def est_mult_5_pas_2(nombre):
    '''renvoie si le nombre est multiple de 5 mais pas de 2
    '''
    pass

#print('25 est un multiple de 5 mais pas un multiple de 2 : ', est_mult_5_pas_2(25))


## EXERCICE 2                     5 pts

def categorie_athle(annee):
    ''''a compléter
    '''
    pass


## EXERCICE 3                              5 pts
def barres(n):
    '''a completer
    '''
    pass

def parenthèses(n):
    '''a completer
    '''

## EXERCICE 4                            5 pts
def algo():
    '''boucle
    '''
    pass








