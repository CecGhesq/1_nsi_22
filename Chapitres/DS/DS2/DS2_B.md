# DS2_B 

[Téléchargez le fichier](DS2_B_python.py) python qui devra être déposé en fin d'évaluation sur Pronote  

## Exercice 1 :(       / 5pts)
Ecrire les prédicats  (réponse booléenne True ou False) directement dans le fichier.
Décommentez ( enlever # ) devant les fonction print pour tester ;

## Exercice 2 : (    /5pts)
Un entraineur de Hand Ball souhaiterait qu’une fonction lui donne la catégorie de la personne selon son année de naissance.

![](hand.jpg)  

On écrira la fonction categorie_HB(annee) en Python qui prend pour argument ‘annee‘ , nombre entier,  année de naissance du sportif.

```python
>>> categorie_HB(2011)
'-12'
```

## Exercice 3 : 
Réalisez les procédures d'affichage ( pas de return, print) en __utilisant la boucle non bornée__ `while`.

1. Affiche 1 ligne avec 2 '^' puis  deux de plus à chaque ligne jusque n lignes.

```python
>>> flex(4)
^^
^^^^
^^^^^^
^^^^^^^^
```
2. Affiche alternativement 'OOOO' puis'NNNN' sur n lignes

```python
>>> ON(6)
OOOO
NNNN
OOOO
NNNN
OOOO
NNNN
```

## Exercice 4 :
Implémenter l’algorithme suivant en Python ( écrire la fonction en Python)qui  demande un entier  strictement positif à l’utilisateur puis, tant que cet entier n’est pas égal à 1, le divise par deux si il est  pair ou le multiplie  par trois et lui ajoute un s’il est impair.

Fonction conjecture_Syracuse  
valeur <-- demande  à l’utilisateur un nombre strictement positif  
TANT QUE valeur différent de 1 :  
______SI valeur est un nombre pair ALORS :  
______________valeur <-- valeur divisée par deux (division entière)  
______SINON :  
______________valeur <-- valeur * 3 + 1  
______Afficher valeur  
Afficher valeur 




