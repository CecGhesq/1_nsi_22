######### SUJET A ################################
## EXERCICE 1     5 pts

# Ecrire les prédicats suivants (réponse booléenne True ou False)  :
def est_negatif(nombre):
    '''renvoie si le nombre est stricitement négatif
    '''
    return nombre < 0

print('-4 est négatif :', est_negatif(-4))

def est_impair(nombre):
    ''' Renvoie si le nombre est impair
    '''
    return nombre % 2 == 1

print('23 est un nombre impair: ',est_impair(23))    # appel à décommenter pour tester

def est_multiple_5(nombre):
    '''renvoie si le nombre est multiple de 5
    '''
    return nombre % 5== 0 
print('25 est un multiple de 5 : ', est_multiple_5(25)) # appel à décommenter pour tester

def est_dans_la_gamme(valeur):
    '''renvoie si la valeur est comprise entre 400 et 800 (valeurs incluses)
    '''
    return valeur>= 400 and valeur <= 800

print('345 est dans la gamme : ',est_dans_la_gamme(345))   # appel à décommenter pour tester

def est_mult_5_pas_2(nombre):
    '''renvoie si le nombre est multiple de 5 mais pas de 2
    '''
    return nombre % 5 == 0 and nombre % 2 != 0

print('25 est un multiple de 5 mais pas un multiple de 2 : ', est_mult_5_pas_2(25))


## EXERCICE 2                     5 pts

def categorie_athle(annee):
    '''renvoie la catégorie de l'athlète en fonction de son année de naissance
    '''
    if annee >= 2016 :
        reponse = 'BB'
    elif annee >=2013 and annee <= 2015 :
        reponse = 'EA'
    elif annee >=2011 and annee <= 2012 :
        reponse = 'PO'
    elif annee >=2009 and annee <= 2010 :
        reponse = 'BE'
    elif annee >=2007 and annee <= 2008 :
        reponse = 'MI'
    elif annee >=2005 and annee <= 2006 :
        reponse = 'CA'
    elif annee >=2003 and annee <= 2004 :
        reponse = 'BE'
    return reponse



## EXERCICE 3                              5 pts

def barres(n):
    '''affiche n lignes avec n barres
    '''
    i = 1
    while i <=  n :
        print(i*'/')
        i = i + 1


def parenthèses(n):
    '''Affiche alternativement une seule paranthèse ou deux sur n lignes
    '''
    i = 1
    while i <=n :
        if i%2 == 1 :
            print('(')
            i = i + 1
        else :
            print('()')
            i = i + 1
    
## EXERCICE 4
def algo():
    '''boucle
    '''
    n = int(input('Quelle est la valeur de n ? '))
    u = n
    while u >= 7:
        if u> 20:
            u = u -7
            print(u)
        else :
            u = u-2
            print(u)
    print(u)