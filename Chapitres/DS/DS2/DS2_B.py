######### SUJET A ################################
## EXERCICE 1     5 pts

# Ecrire les prédicats suivants (réponse booléenne True ou False)  :

def est_pair(nombre):
    ''' Renvoie si le nombre est pair
    '''
    return nombre %2 == 0

print('12 est un nombre pair: ',est_pair(12))    # appel à décommenter pour tester

def est_positif(nombre):
    '''renvoie si le nombre est strictement positif
    '''
    return nombre > 0

print('-4 est positif : ', est_positif(-4))

def est_inside(nombre):
    '''renvoie si le nombre est compris entre 1 et 7 (valeurs incluses)
    '''
    return nombre >=1 and nombre <= 7

print('5.879 est dans la zone: ',est_inside(5.879))   # appel à décommenter pour tester

def est_multiple_3(nombre):
    '''renvoie si le nombre est multiple de 3
    '''
    return nombre % 3 == 0
print('49 est un multiple de 3 : ', est_multiple_3(49)) # appel à décommenter pour tester

def est_mult_2_pas_4(nombre):
    '''renvoie si le nombre est multiple de 2 mais pas de 4
    '''
    return nombre % 4 != 0 and nombre % 2 == 0

print('56 est un multiple de 4 mais pas un multiple de 2 : ', est_mult_2_pas_4(56))


## EXERCICE 2                     5 pts

def categorie_HB(annee):
    ''''a compléter
    '''
    pass


## EXERCICE 3                              5 pts
def flex(n):
    '''Affiche 1 ligne avec 2 '^' puis  deux de plus à chaque ligne jusque n lignes.
    '''
    i = 1
    while i <= n :
        print(2*i*'^')
        i = i+1

def ON(n):
    '''Affiche alternativement 'OOOO' puis'NNNN' sur n lignes
    '''
    i = 1
    while i <= n:
        if i% 2 == 0:
            print('NNNN')
            
        else :
            print('OOOO')
        i = i+1
    
## EXERCICE 4                      5 pts
def conjecture_Syracuse():
    '''au nombre entier donné par l'utilisateur,
    affiche les valeurs prises jusqu'à atteindre la valeur 1
    conjection de Syracuse
    '''
    pass






