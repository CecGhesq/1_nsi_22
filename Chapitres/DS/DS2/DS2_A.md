# DS2_A 

[Téléchargez le fichier](DS2_A_python.py) python qui devra être déposé en fin d'évaluation sur Pronote  

## Exercice 1 :(       / 5pts)
Ecrire les prédicats  (réponse booléenne True ou False) directement dans le fichier.  
Décommentez ( enlever # ) devant les fonction `print` pour tester ;

## Exercice 2 : (    /5pts)
Un entraineur d’athlétisme souhaiterait qu’une fonction lui donne la catégorie de la personne selon son année de naissance.  

![](athle.jpg)

On écrira la fonction categorie_athle(annee) en Python qui prend pour argument ‘annee‘ , nombre entier,  année de naissance de l’athlète.

```python
>>> categorie_athle(2013)
'EA'
```

## Exercice 3 : 
Réalisez les procédures d'affichage ( pas de return, print) en __utilisant la boucle non bornée__  `while`.

1. Affiche n lignes contenant autant de caractère(s) '/' que le numero de ligne.

```python
>>> barres(5)
/
//
///
////
/////
```
2. Affiche alternativement une seule parenthèse ou deux sur n lignes

```python
>>> parenthèses(7)
(
()
(
()
(
()
(
```

## Exercice 4 :
Implémenter l’algorithme suivant en Python ( écrire la fonction en Python).

Variables u , n  

Début  
n <-- à saisir par l'utilisateur  
u <-- prend la valeur de n  
Debut Tant Que u ≥  7  
____Debut SI u > 20  
_________u <--   u – 7  
_________afficher u   
____SINON  
_________u <--   u – 2  
_________afficher u  
____fin SI  
Fin Tant Que  
Fin 



