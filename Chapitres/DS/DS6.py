notes_anna = (12,16,9)

def meilleure_note(notes):
    '''renvoie la meilleure note d'un tuple de notes non vide
    : notes : (tuple)
    @return (int)
    '''
    note_max = notes[0]
    for note in notes :
        if note > note_max :
            note_max = note
    return note_max

def nbre_sup_moyenne(notes_eleve, moyenne_classe):
    '''renvoie le nombre de notes supérieures à la moyenne de classe
    : notes_eleve : (tuple)
    : moyenne_classe : (float)
    @return (int)
    '''
    nb_notes_sup = 0
    for note in notes_eleve :
        if note > moyenne_classe :
            nb_notes_sup = nb_notes_sup + 1
    return nb_notes_sup

championnat_europe_finalistes = {1994 : ("Suède", "Russie") ,
                      1996 : ("Russie", "Espagne"),
                      1998 : ("Suède", "Espagne"),
                      2000 : ("Suède", "Croatie"),
                      2002 : ("Suède", "Allemagne"),
                      2004 : ("Allemagne", "Slovénie"),
                      2006 : ("France", "Espagne"),
                      2008 : ("Danemark", "Croatie"),
                      2010 : ("France", "Croatie"),
                      2012 : ("Danemark", "Serbie"),
                      2014 : ("France" , "Danemark"),
                      2016 : ("Allemagne", "Espagne"),
                      2018 : ("Espagne", "Suède"),
                      2020 : ("Espagne", "Croatie"),
                      2022 : ("Suède" , "Espagne")}

def nombre_champion(dictionnaire , pays):
    '''renvoie le nombre de fois où un pays a gagné l'euro c'est à dire
    lorsque le pays est cité en premier dans le couple des finalistes
    : dictionnaire : (dict)
    : pays : (str)
    @return (int)
    '''
    n_gagne = 0
    for finale in dictionnaire.values():
        if pays == finale[0]:
            n_gagne = n_gagne + 1
    return n_gagne

def annee_gagne(dictionnaire , pays):
    '''renvoie la liste des années où un pays a gagné
    : dictionnaire : (dict)
    : pays : (str)
    @return (list)
    '''
    liste_annee = []
    for finale in dictionnaire.items():
        if pays == finale[1][0]:
            liste_annee.append(finale[0])
    return liste_annee
