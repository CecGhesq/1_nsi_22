notes_hakim = (12,16,18,15)

def nbre_inf_moyenne(notes_eleve, moyenne_classe):
    '''renvoie le nombre de notes inférieures à la moyenne de classe
    : notes_eleve : (tuple)
    : moyenne_classe : (float)
    @return (int)
    '''
    nb_notes_inf = 0
    for note in notes_eleve :
        if note < moyenne_classe :
            nb_notes_inf = nb_notes_inf + 1
    return nb_notes_inf

elections_presidentielles = { 1965 : ('Charles De Gaulle', 'François Mitterand'),
                               1969 : ('Georges Pompidou' , 'Alain Poher'),
                               1974 : ('Valéry Giscard d Estaing', 'François Mitterand'),
                               1981 : ('François Mitterand', 'Valéry Giscard d Estaing'),
                               1988 : ('François Mitterand', 'Jacques Chirac'),
                               1995 : ('Jacques Chirac', 'Lionel Jospin'),
                               2002 : ('Jacques Chirac', 'Jean-Marie Le Pen'),
                               2007 : ('Nicolas Sarkozy', 'Ségolène Royal'),
                               2012 : ('François Hollande', 'Nicolas Sarkozy'),
                               2017 : ('Emmanuel Macron' , 'Marine Le Pen'),
                               2022 : ('Emmanuel Macron' , 'Marine Le Pen')
                               }

def nbre_victoire(dictionnaire, candidat):
    '''renvoie le nombre de victoire d'un candidat aux élections présidentielles
    : dictionnaire : (dict)
    : candidat : (str)
    @return (int)
    '''
    nb_victoire  = 0
    for candidats in dictionnaire.values():
        if candidats[0] == candidat :
            nb_victoire = nb_victoire + 1
    return nb_victoire
 
def liste_vic(dictionnaire , candidat):
    '''renvoie la liste des années où un candidat a remporté l'élection
    : dictionnaire : (dict)
    : candidat : (str)
    @return (list)
    '''
    liste_annee = []
    for finale in dictionnaire.items():
        if candidat == finale[1][0]:
            liste_annee.append(finale[0])
    return liste_annee