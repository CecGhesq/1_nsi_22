def rendu_monnaie_centimes(s_due, s_versee):
    ''' renvoie la liste de pièces composant la somme rendue
    :s_due : (int) somme à payer
    :s_versee : (int) somme versée ( supérieure à la somme due )
    @ return : (list) liste de la somme rendue
    >>> rendu_monnaie_centimes(700,700)
    []
    >>> rendu_monnaie_centimes(112,500)
    [200, 100, 50, 20, 10, 5, 2, 1]
    '''
    pieces = [1, 2, 5, 10, 20, 50, 100, 200]
    rendu = ...
    a_rendre = ...
    i = len(pieces) - 1
    while a_rendre > 0 :
        if pieces[i] <= a_rendre :
            rendu.append(... )
            a_rendre = ...
        else :
            i = ...
    return rendu