$(document).ready(function() {
	/*Déclaration des variables*/
	var compteur;
	var valeur;
	var casecourante = 10;
	/*Adapter à l'écran*/
	$(window).resize(function () {
		setTimeout(window.location="../index.html",2000);
	});
	/*Empêche le menu contextuel*/
	$(document).on("contextmenu",function (event) {event.preventDefault();});
	/*Empèche de zoomer avec la sourie*/
	$(document).on('DOMMouseScroll', function(event){
		event.stopPropagation();
		event.preventDefault();
		event.cancelBubble = false;
		return false;
	}, false);
	initialisation();
	
	/*Fonction qui initialise le programme*/
	function initialisation() {
		creationgrille();
		entreroptions();
		fonctionvaliderbouton();
		fonctiontesterbouton();
	}
	/*Fonction qui crée une case.*/
	function creationcase(i, nomligne) {
		for (var j = 0; j <= 9; j++) {
					nombre = i*10+j;
					if (i==0) {
						$("#colonne"+i).append("<div id = 'case"+nombre+"'> "+ j +"</div>");
						$("#case"+nombre).css({width:6+"vw", height:6+"vh", textAlign:"center", lineHeight:6+"vh"});
					}
					else {
						$("#colonne"+i).append("<div id = 'case"+nombre+"'> </div>");
						$("#case"+nombre).css({width:6+"vw", height:6+"vh", border: "1px black solid", textAlign:"center", lineHeight:6+"vh"});
					}	
		}		
	}
	/*Fonction qui crée la grille mémoire*/
	function creationgrille() {
		for (var i = 0; i <= 10; i++) {
			$("#grille").append("<div id = 'colonne"+i+"'></div>");
			$("#colonne"+i).css({width:6+"vw", height:77+"vh", display:"flex", flexDirection:"column", justifyContent:"space-evenly", alignItems:"center"});
			if (i==0) {
				$("#colonne"+i).append("<div class = 'etiquette'></div>");
				$(".etiquette").css({width:6+"vw", height:6+"vh"});
			}
			else {
				nombre = i-1
				$("#colonne"+i).append("<div class = 'etiquette'>"+ nombre*10 +"</div>");
				$(".etiquette").css({width:6+"vw", height:6+"vh", textAlign:"center", lineHeight:6+"vh"});
			}			
			creationcase(i, "#colonne"+i);
		}
	}
	/*Entrer les options de seclect*/
	function entreroptions() {
		for (var i = 0; i <= 99; i++) {
			$("#caseselectionne").append("<option>"+ i +"</option>");
		}
	}
	
	/*Activation bouton valider.*/
	function fonctionvaliderbouton() {
		boutonvalider = document.getElementById("valider");
		boutonvalider.addEventListener("click", extrairedonnees);
	}
	/*Fonction qui va extraire les données du formulaire.*/
	function extrairedonnees() {
		if ($("#valeurcase").val()[0] == "#") {
			if (!isNaN(parseInt($("#valeurcase").val().substr(1,$("#valeurcase").val().length-1)))) {
				ecriredonnees(parseInt($("#caseselectionne").val())+10, $("#valeurcase").val().substr(1,$("#valeurcase").val().length-1),"blue", "#AAAAAA");
				$("#affichage").html("");
			}
			else {
				$("#affichage").html("Mauvaise entrée");
			}	
		}
		else {
			if ($("#valeurcase").val().length == 3){
				if (verifierdonnees($("#valeurcase").val())) {
					ecriredonnees(parseInt($("#caseselectionne").val())+10, $("#valeurcase").val(),"black", "#006688");
					$("#affichage").html("");
				}
				else {
					$("#affichage").html("Mauvaise entrée");
				}
			}
			else {
				$("#affichage").html("Mauvaise entrée");
			}
		}
	}
	/*Fonction qui va vérifier les données extraites.*/
	function verifierdonnees(test) {
		valeursvalides = [0, 1, 2, 3, 4, 5, 6];
		for (var i = 0; i<=6 ; i++) {
			if (test[0] == valeursvalides[i]) {
				if (test[0] == 3) {
					if ((test[1]==0&&test[2]==0)||(test[1]==0&&test[2]==1)||(test[1]==9&&test[2]==9)) {
						return true;
					}
				}
				else {
					if (test[0] == 4) {
						if ((test[1]==0&&(test[2]==1||test[2]==2))||(test[1]==1&&(test[2]==0||test[2]==2))||(test[1]==2&&(test[2]==1||test[2]==0))) {
							return true
						}
					}
					else {
						if ((test[1]>=0&&test[1]<=9)&&(test[2]>=0&&test[2]<=9)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	function verifierdonnees1(test) {
		valeursvalides = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
		if ((test[0] == 4)&&(test[3] == ",")) {
			if ((test[1]>=0&&test[1]<=9)&&(test[2]>=0&&test[2]<=9)&&(test[4]>=0&&test[4]<=9)&&(test[5]>=0&&test[5]<=9)) {
				return true;
			}
		}
		return false;
	}
	
	/* Fonction qui reinitialise.*/
	function reinitialiser() {
		window.location.reload();
	}
	
	/* Rafraichir la grille après avoir appuyer sur le bouton tester*/
	function rafraichir() {
		casecourante = 10;
		for (var i = 10; i<=109; i++) {
			if (($("#case"+i).css("backgroundColor")=='rgb(0, 255, 255)')&&($("#case"+i).css("color")=='rgb(0, 0, 0)')&&($("#case"+i).html()!=" ")) {
				$("#case"+i).css({backgroundColor:'rgb(0, 102, 136)'});
			}
			if (($("#case"+i).css("backgroundColor")=='rgb(0, 255, 255)')&&($("#case"+i).css("color")=='rgb(0, 0, 255)')) {
				$("#case"+i).css({backgroundColor:'rgb(170, 170, 170)'});
			}
			if (($("#case"+i).css("backgroundColor")=='rgb(0, 255, 255)')&&($("#case"+i).html()==' ')) {
				$("#case"+i).css({backgroundColor:'rgb(204, 204, 204)'});
			}			
		}
		$("#affichecompteur").html("");
		$("#afficheA").html("");
		$("#afficheB").html("");
		$("#afficheR").html("");
	}	
	
	/*Fonction qui va écrire les données dans les cases mémoires.*/
	function ecriredonnees(numerocase, valeurcase,couleurpolice, couleurfond) {
		$("#case"+numerocase).text(valeurcase).css({color:couleurpolice, backgroundColor:couleurfond});
	}
	
	/*Activation bouton tester*/
	function fonctiontesterbouton() {
		boutontester = document.getElementById("tester");
		boutontester.addEventListener("click", lireprogramme);
		boutonreinitialiser = document.getElementById("reinitialiser");
		boutonreinitialiser.addEventListener("click", reinitialiser);
	}

	/*Fonction qui lit le programme*/
	function lireprogramme() {
		compteur = 0
		if (($("#case10").css("backgroundColor")=='rgb(0, 255, 255)')&&(compteur==0)) {
			rafraichir();
		}
		programme = setInterval(programmeprincipal, 5000);
	}
	
	/*Modifier compteur*/
	function modifiercompteur() {
		compteur ++;
		$("#affichecompteur").html(compteur);
		return compteur;
	}
	
	/*Fonction qui lit une cellule mémoire*/
	function lecturecellulememoire(a) {
		return $("#case"+a).html();
	}
	
	/*Fonction qui analyse la valeur de la case mémoire*/
	function analysevaleurmemoire(valeurparametre) {
		var cellule = parseInt(valeurparametre.substr(1,2))+10;
		switch (valeurparametre[0]) {
			case "0": 
					$("#afficheA").html($("#case"+cellule).html());
					$("#affichage").html("copie la valeur de la mémoire "+valeurparametre[1]+valeurparametre[2]+" dans le registre A");
					casecourante++;
					break;
			case "1": 
					$("#afficheB").html($("#case"+cellule).html());
					$("#affichage").html("copie la valeur de la mémoire "+valeurparametre[1]+valeurparametre[2]+" dans le registre B");
					casecourante++;
					break;
			case "2":
					$("#case"+cellule).html($("#afficheR").html()).css({color:"blue", backgroundColor:"rgb(170, 170, 170)"});					
					$("#affichage").html("copie la valeur du registre R dans la mémoire "+valeurparametre[1]+valeurparametre[2]);
					casecourante++;
					break;
			case "3": 
					if (valeurparametre[2]=="0") {
						$("#afficheR").html(parseInt($("#afficheA").html())+parseInt($("#afficheB").html()));
						$("#affichage").html("Additionne les valeurs des registres A et B et le copie dans le registre R.");
					}
					else{
						if (valeurparametre[2]=="1") {
							$("#afficheR").html($("#afficheA").html()-$("#afficheB").html());
							$("#affichage").html("Réalise la soustraction A - B et la copie dans le registre R.");
						}
						else {
							$("#affichage").html("Ne fait rien.");
						}
					}
					casecourante++;					
					break;
			case "4":
					if (valeurparametre[1] == 0&&valeurparametre[2] == 1) {
						$("#afficheB").html($("#afficheA").html());
					}
					if (valeurparametre[1] == 1&&valeurparametre[2] == 0) {
						$("#afficheA").html($("#afficheB").html());
					}
					if (valeurparametre[1] == 0&&valeurparametre[2] == 2) {
						$("#afficheR").html($("#afficheA").html());
					}
					if (valeurparametre[1] == 2&&valeurparametre[2] == 0) {
						$("#afficheA").html($("#afficheR").html());
					}
					if (valeurparametre[1] == 2&&valeurparametre[2] == 1) {
						$("#afficheB").html($("#afficheR").html());
					}
					if (valeurparametre[1] == 1&&valeurparametre[2] == 2) {
						$("#afficheR").html($("#afficheB").html());
					}
					$("#affichage").html("Copie la valeur de "+valeurparametre[1]+" dans "+valeurparametre[2]+".");
					casecourante++;
					break;
			case "5":
					if ((valeurparametre[1] == "9")&&(valeurparametre[2] == "9")) {
						clearInterval(programme);
						$("#affichage").html("Réalise un saut vers la mémoire 99");
						$("#case109").animate({backgroundColor:"#00FFFF"},1500);
					}
					else {
						casecourante = parseInt(valeurparametre.substr(1,2))+10;
						$("#affichage").html("Réalise un saut vers la mémoire "+(casecourante-10));
					}
					break;
			case "6":
					if (parseInt($("#afficheR").html())>0) {
						$("#affichage").html("Réalise un saut vers la mémoire si le regitre R est stritement positif.");
						casecourante = parseInt(valeurparametre.substr(1,2))+10;		
					}
					else {
						casecourante++;
					}
					break;
			case " " :
					clearInterval(programme);
		}
	}
	
	/*function qui analyse la valeur de la case mémoire.*/
	function analyseinstruction() {
		valeur = lecturecellulememoire(casecourante);
		$("#case"+casecourante).animate({backgroundColor:"#00FFFF"},1500);
		if ($("#case"+casecourante).css("color") == "rgb(0, 0, 0)") {
			analysevaleurmemoire(valeur);
		}
		else {
			$("#affichage").html("Variable");
			casecourante++;
		}
	}
	
	/*Programme principal*/
	function programmeprincipal() {
		compteur = modifiercompteur();
		analyseinstruction();
	}
});