import cercle

def double_aire(rayon):
    return cercle.aire(rayon)*2
