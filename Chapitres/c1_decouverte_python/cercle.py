#### importations
from math import pi
#####
###ex 6

def perimetre(rayon):
    '''renvoie le périmètre du cercle
    '''
    return 2*pi*rayon


def aire(rayon):
    '''renvoie la surface
    '''
    return pi * rayon**2
