def carre(n):
    '''renvoie le carré de n
    '''
    return n**2

def perimetre_rect(l,L):
    '''renvoie le périmètre du rectangle
    '''
    return 2*(l + L)

def moyenne(note1, note2, note3):
    '''renvoie la moyenne de 3 notes
    '''
    return (note1 + note2 + note3)/3

def moyenne_pond(n1,c1,n2,c2):
    '''renvoie la moyenne pondérée des 2 notes
    '''
    return (n1*c1+n2*c2)/(c1+c2)


