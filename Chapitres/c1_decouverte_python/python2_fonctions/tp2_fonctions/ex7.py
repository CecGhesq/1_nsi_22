from math import pi

def cube(nombre):
    """renvoie le cube d'un nombre"""
    return nombre **3

def volume_sphere(rayon):
    return 4*pi / 3 * cube(rayon)

