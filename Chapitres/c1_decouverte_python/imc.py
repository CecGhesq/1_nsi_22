def calcul_imc(masse, taille) :
    '''renvoie la valeur de l'imc
    '''
    return masse/taille**2

def calcul_imc_bis(masse, taille) :
    '''renvoie la valeur de l'imc
    '''
    imc = masse/taille**2
    return round(imc,1)

### ex 4
al = 299792458 * 31557600 / 1000 # 1al en km

def al_km(distance_al):
    '''renvoie la distance donnée en al en km
    '''
    return distance_al * al

