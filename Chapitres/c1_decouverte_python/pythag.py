##ex 5
####importations####
import math

######
def hypotenuse(a,b) :
    '''renvoie la valeur de l'hypotenuse
    '''
    c = math.sqrt(a**2 + b**2)
    return c