---
title: "Chapitre 2 Représentations des entiers en machine"
subtitle : "Les bases de numération"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

L'ensemble des entiers naturels $`\mathbb{N}`$ regroupe le 0 et les nombres positifs $`{0;1;2;3;4 \dots }`$.

La notation décimale que nous sommes habitués à utiliser depuis notre enfance n'est pas la seule façon de réprésenter ces nombres, elle est de plus inadaptée aux machines informatiques.

# Exemple de la base décimale
## Notation décimale  

Pour comprendre le fonctionnement du binaire, et des systèmes de comptage en général (plus communément appelés bases), nous allons revoir la base 10.

Tout le monde sait compter en base 10 (décimal). Mais comment ça marche ? Comment est construit notre système ? Comment avons-nous appris à compter à l'école ?
Il y a 10 chiffres : 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.
Avec ces derniers, on peut compter jusqu'à 9.

Et si l'on veut aller au-delà de 9, **il faut changer de rang**.
Cela signifie que si le rang des unités est plein, il faut passer à celui des dizaines, puis des centaines,      milliers ...

Par exemple : à 19, le rang des unités est "saturé" (plein), car il contient le chiffre 9, et il n'y a pas (dans la base 10) de valeur plus élevée. Il faut donc incrémenter le rang périphérique puis réinitialiser l'état de celui des unités. Ce qui signifie : j'ai 19, je ne peux pas mettre plus de 9 à droite, donc j'ajoute 1 à celui de gauche et je remets à zéro celui de droite.  

![185](./img/185.png)

Le nombre entier va être composé de rangs (unités, dizaines, centaines, etc). *Chaque rang vaut le rang précédent multiplié par l'indice de la base*. Une centaine vaut dix dizaines, et une dizaine vaut 10 unités.  
Par exemple, dans l'image ci-dessus, on peut voir le nombre  $`\bf 185_{10}`$ (ici, le 10 signifie qu'il s'agit d'un nombre, en base 10). Dans ce nombre, on peut voir trois rangs : centaines, dizaines et unités.  
Pour n'importe quelle base, la valeur d'un rang est égale à $`\bf b_{n}`$, où b est l'indice de la base (ici, 10) et n la position du rang. Ici, les unités ont la position 0, les dizaines la position 1 et les centaines la position 2. Nous pouvons donc écrire :  

185 =  $`1\times{\color{red}{10^{2}}}+ 8\times{\color{red}{10^{1}}}+ 5 \times {\color{red}{10^{0}}}`$

Nous avons ainsi **décomposé**  185 en **puissance de 10**(unités, dizaines, centaines, etc).

Un nombre est égal à la somme des valeurs de ses rangs, et on peut décomposer n'importe quel nombre en puissance de sa base.  

__📝Application__ : _décomposer 345 632 en base 10_ 

```python


```

## Décomposition généralisée dans une base donnée

La représentation décimale d'une nombre __N__ est la suite ordonnée de _n_ chiffres : $`\bf a_{n-1} a_{n-2}\dots a_{1} a_{0}`$

Comme on l'a vu on peut donc décomposer cette représentation ainsi :  

$`\bf N = a_{n-1} \times 10^{n-1} + a_{n-2} \times10^{n-2}+ \dots +a_2 \times10^2 +a_1 \times10^1 +a_0 \times10^0`$

Par exemple pour le nombre 253 :  
$`a_{2} = 2, a_{1} = 5, a_{0} = 3`$ --> n = 3 chiffres sont nécessaires.

On peut généraliser cette décomposition à n'importe quelle base __B__ de numération :  
$`\bf N=\sum\limits_{{i=0}}^{n-1}{a_i \times B^i}`$

soit :  $`\boxed{\bf{N =a_{n-1} B^{n-1} + a_{n-2} B^{n-2}+ \dots +a_2 B^2 +a_1 B^1 +a_0 B^0}}`$

La représentation associée sera notée : $`\bf{(a_{n-1}a_{n-2}\dots a_{1}a_{0})}_{B}`$ 

__📐 Point mathématique :__  
_Le signe $`\sum`$ se lit "sigma". Il décrit une série de plusieurs additions._  
_Par exemple_  $`N=\sum\limits_{{i=1}}^{4}{2n}`$ _se lit "somme pour __n__ allant de 1 à 4 de __2n__._  
 _Cela signifie que l'on fait prendre au nombre n toutes les valeurs entières entre 1 et 4 et qu'on fait la somme des termes 2n :_   $`N = 2\times 1 + 2\times 2 +2\times 3 +2\times 4 = 14`$

\newpage

# Le binaire
## Définition
Le binaire est le système de comptage des ordinateurs. Pourquoi le binaire et pas le décimal comme les humains ? Et bien c'est très simple : un ordinateur est composé de circuits électroniques, et donc de composants électriques. Le plus simple pour compter est donc d'utiliser un système en base 2 (le binaire) car on peut représenter ses deux valeurs possibles (0 et 1) par un signal électrique : 1, la tension est délivrée, 0, elle ne l'est pas (c'est la version simple).
Chaque rang en binaire ne peut avoir que **deux valeurs** (binaire = base 2) différentes : **0 ou 1**. Pour la base 10, chaque rang représente une puissance de 10, pour la base 2, chaque rang occupe une puissance de 2. Voici comment compter en binaire jusqu'à 10 :  

| $`2^{3}`$ | $`2^{2}`$ | $`2^{1}`$ | $`2^{0}`$ | expression en base 10 |
|:---------:|:---------:|:---------:|:---------:|:---------------------:|
|           |           |           |     0     |           0           |
|           |           |           |     1     |           1           |
|           |           |     1     |     0     |           2           |
|           |           |     1     |     1     |           3           |
|           |     1     |     0     |     0     |           4           |
|           |     1     |     0     |     1     |           5           |
|           |     1     |     1     |     0     |           6           |
|           |     1     |     1     |     1     |           7           |
|     1     |     0     |     0     |     0     |           8           |
|     1     |     0     |     0     |     1     |           9           |
|     1     |     0     |     1     |     0     |           10          |


__📝Application 2__ 
* Quelle est la décomposition de 9 en base 2?  

```
 
```

On remarquera la syntaxe d'écriture $`\bf (....)_{2}`$ : lorsqu'on décompose une valeur décimale dans une base, on place la base de destination en indice

_Remarque_ : Attention, de nombreuses notations coexistent comme $`1011_{2} ou \overline{1011}_{2`} ou encore \overline{1011}^{2}`$.  

__📝Application 3__ 
* Quelle est  le développement  de  $`\overline{1101}`$ qui est en base 2 ainsi que son expression en base décimale 

```
 
```

**On remarquera que le dernier chiffre est celui de poids le plus faible

** Son expression en base 10 sera égale à  8 + 4 + 0 + 1 = 13

Remarque :  il peut être intéressant de connaître quelques puissances courantes de 2 : 

|$`\bf n`$|0|1|2|3|4|5|6|7|8|  
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|  
|$`\bf 2^{n}`$|1|2|4|8|16|32|64|128|256|  

## Quelques définitions en informatique  

> 📢**BIT** : unité fondamentale du système de numération. Un bit ne peut prendre que deux valeurs qu'on note 0-1 ou False-True ou Bas-Haut.  
> 📢**MOT NUMERIQUE** : Ensemble de plusieurs bits. Le bit de poids faible est toujours à droite.  
> 📢**BYTE** : Plus petit mot numérique adressable à un ordinateur. Le standard du BYTE est devenu l'octet (8 bits à la fois) mais d'autres possibilités existent encore pour certains systèmes spécifiques. En français, on devrait dire multiplet mais on ne peut pas dire que son emploi soit courant ...  
> 📢**OCTET** : Un octet correspond à un **mot numérique de 8 bits**. En anglais, on dit eight-bit byte mais comme l'octet est devenu la valeur la plus courante du byte, on trouve beaucoup de documentation où on note simplement byte plutôt que eight-bit byte.  

![explication_octet](./img/explication_octet.PNG)

> 📢**A RETENIR**
Tout entier naturel $`a`$ peut se décomposer dans la base $`2`$ de manière unique en écrivant :  
$`a= a_n 2^n+a_{n-1} 2^{n-1}+\cdots + a_0`$  
avec $`a_n>0\: et\:\forall i\:\in \:[0;n],\; a_i = 0 \text{ ou }1`$  

On dit que $`\left(a_n\cdots a_0\right)_2`$  est la représentation en base $`2`$ de $`a`$.  
On convient que la décomposition de $`0`$ dans la base $`2`$ est $`\left(0\right)_2`$

##  Nombres de valeurs possibles

* Combien de valeurs peut-on coder avec 1 bit ?  
2 valeurs (0 et 1)

* Combien de valeurs peut-on coder avec 2 bits ?  
4 valeurs (00 01 10 et 11)
* Combien de valeurs peut-on coder avec 3 bits ?  
8 valeurs (000 001 010 011 100 101 110 111)
\newpage

> 📢**A RETENIR**  
 __n__ bits permettent de représenter tous les entiers naturels compris entre __0__  et $`\bf 2^{n}-1`$

En effet pour n = 3 bits, on atteint $`\bf 2^{3}`$ = 8 valeurs mais la valeur maximale atteinte est 7 ( les nombres sont compris entre 0 et 7).  

## Convertir un nombre décimal en binaire

Il existe bien sûr plusieurs méthodes de conversion, mais nous allons commencer par la plus simple et la plus rapide. Il s'agit de la méthode euclidienne.

Cette méthode, en plus d'être facile à utiliser en programmation (c'est un algorithme) est une des meilleures lorsqu'il s'agit de traiter les grands nombres.

Voici la méthode :

   * On prend le nombre en base 10 (forme normale).
   * On le divise par 2 et on note le reste de la division (soit 1 soit 0)
   * On refait la même chose avec le quotient précédent, et on met de nouveau le reste de côté.
   * On réitère la division, jusqu'à ce que le quotient soit 0.
   * Le nombre en binaire apparaît alors : il suffit de prendre tous les restes de bas en haut.
Si on reprend le nombre 185 en décimal cela donne :  

![divi](./img/div_euclidienne.PNG)

Attention, il faut bien lire de bas en haut !

185 en base 10 vaut donc 10111001 en binaire.

**Application 4 :**  
 ```
 















```
\newpage

# Ecriture en base 16 (hexadécimale)

## Intérêt

Pour la base 16 les symboles utilisés sont les 10 chiffres et les 6 premières lettres de notre alphabet.  

|alphabet hexadécimal|0|1|2|3|4|5|6|7|8|9|A|B|C|D|E|F|  
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|  
|équivalence en décimal|0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|  

Cette notation simplifie la représentation des nombres car moins de symboles seront nécessaires pour représenter un nombre qu'en décimal par exemple.  
Elle sera notamment utilisée pour représenter les données stockées en machine de manière plus lisible pour un humain.

## Décomposition

On décompose cette représentation ainsi :  

$`\bf N = a_{n-1} \times 16^{n-1} + a_{n-2} \times16^{n-2}+ ..... +a_2 \times16^2 +a_1 \times16^1 +a_0 \times16^0`$

Le principe de passage de l'hexadécimal vers le décimal est donc le même que celui du binaire vers le décimal si ce n'est que l'on remplacera les lettres utilisées par les valeurs décimales correspondantes.

$`\bf (F2)_{16} = 15\times {\color{red}16^{1}} + 2\times {\color{red}16^{0}} = 240 + 2= 242`$ 

__📝Application__ : _décomposer $`\bf (10C)_{16}`$ en base 10_ 

```
 

```

## De l'hexadécimal vers le binaire

Tout groupe de 4 bits peut être représenté par un seul caractère hexadécimal.

En effet :

* la valeur minimale avec 4 bits en binaire est $`(0000)_{2}`$ soit   $`\bf (0)_{16}`$  
* la valeur maximale est $`(1111)_{2}`$ soit   $`\bf (F)_{16}`$  

__📌 Méthode :__  

> Pour passer du binaire à l'hexadécimal, on regroupera donc les paquets par 4 

__📝Application__ : $`\bf (\overbrace{1011}^{\text{B}} \overbrace{1110}^{\text{E}})_{2}  = (BE)_{16}`$ 

# 💾 Utilisation des bases numériques en Python

## Préfixes et notations

En Python, on peut directement écrire les nombres en binaire en utilisant le préfixe __`0b`__

```python
>>> 0b101010
42
```

En effet :  $`{(101010)}_{2}=42`$  

Pour les nombres hexadécimaux : le préfixe utilisé est __`0x`__

```python
>>> 0x2A
42
```

Là aussi :  $`{(2A)}_{16}=42`$

##  Les opérateurs de comparaison

Nous souhaitons faire la comparaison du contenu de 2 représentations nommées X et Y grâce à la syntaxe suivante : `X operateur Y`  
Voici, ci-dessous le lien entre les symboles mathématiques et les opérateurs python correspondants
   
|Symbole mathématique|Opérateur python|  
|--------------------|----------------|  
|$`=`$                 |`==`            |  
|$`\not =`$            |`!=`            |  
|$`>`$                 |`>`             |  
|$`\geq`$              |`>=`            |  
|$`<`$                 |`<`             |  
|$`\leq`$              |`<=`            |  

Par exemple l'expression  `12 > 13` sera évaluée comme la réponse à la question *"Est-ce que le nombre entier 12 est supérieur au nombre entier 13 ?"*  

```python
>>> 12 > 13
False
```

L'évaluation de telles expressions conduit nécessairement à deux réponses possibles Vrai ou Faux : en langage Python `True`  ou `False`.


```python
>>> 14 >= 13
True
```

Ces deux valeurs particulières appartiennent à un nouveau type de données : les __booléens__ (_cette notion sera approfondie ultérieurement_)

```python
>>> type(True)
<class 'bool'>
```

On peut notamment vérifier l'égalité des représentations du nombre 42 en bases binaire et hexadécimale.

```python
>>> 0b101010 == 0x2A
True
```

SOURCES :  

* cours Patrice Thibaud NSI 1ère Université de Lille
* https://pixees.fr/informatiquelycee/n_site/nsi_prem_base_2_16.html ;
* https://infoforall.fr/fiches/fiches-act040.html
* https://fr.wikipedia.org/wiki/Base_(arithm%C3%A9tique)
* cours algorithmique et programmation MPSI Douai Lycée Chatelet
* cours algorithmique et programmation MPSI Arras Lycée Gambetta
