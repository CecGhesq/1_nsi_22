let temp, celsius, fahrenheit, choix;

function FC(t) {
	return ((t - 32) *  5 / 9).toFixed(2);
}

function CF(t) {
	return ((9/5)*t + 32).toFixed(2);
}

temp = parseFloat(window.prompt("Entrez la valeur de la température "));

choix = window.prompt("Cette température est-elle en °C ou en °F?", "°C, °F"))


if (choix == "°C"){
	celsius = temp;
	fahrenheit = CF(temp);
	window.alert(temp+"°C"+"="+fahrenheit+"°F")
}	
	
else{
	fahrenheit = temp;
	celsius = FC(temp);
	window.alert(temp+"°F"+"="+celsius+"°C")
}



