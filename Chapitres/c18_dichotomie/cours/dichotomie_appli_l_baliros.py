# Créé par laurent, le 08/02/2022 en Python 3.7
from tkinter import*
from random import randint
import time
largeur=1600
hauteur=800

def dicho(L,x) :
    global nbE
    liste=[]
    debut, fin = 0, len(L)-1
    liste.append((L,x,debut,fin,None,nbE,None))
    liste.append((L,x,debut,fin,None,nbE,None))
    while debut <= fin :
        milieu=(debut+fin)//2
        nbE+=1
        liste.append((L,x,debut,fin,milieu,nbE,None))

        if x == L[milieu] :
            liste.append((L,x,debut,fin,milieu,nbE,True))
            return liste
        elif x < L[milieu] :
            fin = milieu-1
            liste.append((L,x,debut,fin,milieu,nbE,"a",None))
        else :
            debut = milieu+1
            liste.append((L,x,debut,fin,milieu,nbE,"a",None))
    liste.append((L,x,debut,fin,-1,nbE,False))
    return liste
def jouer():
    global rep,liste,rang

    def bouger():
        global id,rang
        bou5["command"]=stop
        can.delete("all")
        rang+=1
        if rang<len(rep):
            liste=rep[rang]
            dessiner(liste)
            id=can.after(2000,bouger)
        else:
            liste=rep[len(rep)-1]
            dessiner(liste)
            test=False

    def testMarche():
            global enMarche,rang
            if not enMarche:
                    enMarche=True
                    bouger()

    def stop():
            global enMarche
            can.after_cancel(id)
            enMarche=False

    def dessiner(liste):
        global rang,rep
        a=(largeur-200)/64
        H=hauteur/4+a/2
        H2=H-a

        def rectangle(i,Width,Fill,Outline):
            can.create_rectangle(100+a*(i%64),hauteur/4+a*(i//64),100+a*(i%64+1),hauteur/4+a+a*(i//64),width=Width,fill=Fill,outline=Outline)
        def texte(i,H,Text,Fill):
            can.create_text(100+a/2+a*(i%64),H+a*(i//64),text=Text,fill=Fill,font="Arial "+str(hauteur//84))

        def comparaison():
            can.create_text(largeur/2,hauteur/15,text="nombre recherché:  "+str(liste[1])+" en "+str(liste[5])+ " coup(s)",fill="#DABEA0",font="Arial "+str(hauteur//22))

        nbCherche=can.create_text(largeur/2,hauteur/6,text="",fill="red",font="Arial "+str(hauteur//18))



        comparaison()
        for i in range(1088):
            rectangle(i,1,"ivory2","ivory3")
            texte(i,H,liste[0][i],"black")

        for i in range(1088):
                if i <liste[2] or i>liste[3]:

                        rectangle(i,1,"ivory3","ivory4")
                        texte(i,H,liste[0][i],"white")
        if liste[-1]==True:

            rectangle(liste[4],1,"ivory2","ivory3")
            can.create_rectangle(100+a*(liste[4]%64)-a,hauteur/4+a*(liste[4]//64)-a,100+a*(liste[4]%64+1)+a,hauteur/4+a+a*(liste[4]//64)+a,width=3,fill="ivory2",outline="red")

            can.create_text(100+a/2+a*(liste[4]%64),H+a*(liste[4]//64),text=liste[0][liste[4]],fill="red",font="Arial "+str(hauteur//34))




        elif liste[4]!=None and liste[4]!=-1:
            rectangle(liste[4],3,"","red")
            texte(liste[4],H,liste[0][liste[4]],"red")
            if liste[1]>liste[0][liste[4]]:
                can.itemconfigure(nbCherche,text=str(liste[1])+"  >  "+str(liste[0][liste[4]]))
            elif liste[1]<liste[0][liste[4]]:
                can.itemconfigure(nbCherche,text=str(liste[1])+"  <  "+str(liste[0][liste[4]]))

            if liste[-2]=="a":
                for i in range(liste[2],liste[3]+1):

                    rectangle(i,2,"","green")



    bouger()


def Partie():
    global nbE,listenb,rep,liste,rang
    test=True
    nbE=0
    id=None
    enMarche=False
    listenb=[]

    while len(listenb)!=1088:
        nb=randint(-1088,1088)
        if nb not in listenb:
            listenb.append(nb)
    listenb=sorted(listenb)

    bou1["state"]=DISABLED
    bou4["state"]=NORMAL
    bou5["state"]=NORMAL
    bou6["state"]=NORMAL

    x=randint(-1200,1200)

    rep=dicho(listenb,x)
    liste=rep[0]
    rang=0

def recom():
    can.delete("all")
    Partie()
    jouer()



fen=Tk()
xmax = fen.winfo_screenwidth()
ymax = fen.winfo_screenheight()
x0 =  xmax/2-(largeur/2)
y0 =  ymax/2-(hauteur/2)
fen.geometry("%dx%d+%d+%d" % (largeur,hauteur, x0, y0))
tex1 = Label(fen, text="\n <<oo>>  recherche dichotomique  <<oo>>",font ="Arial 35",fg="ivory4")
tex1.pack()
can=Canvas(height=hauteur-200,width=largeur)
can.pack()


bou1= Button(fen, text='AVANT DE DEMARRER', bg="ivory3", fg="white",font="Arial 20",bd=0,command=Partie)
bou1.pack(side=LEFT,expand=YES, fill=BOTH)
bou4= Button(fen, text='START',  bg="ivory3", fg="white",font="Arial 20",bd=0,state =DISABLED,command=jouer)
bou4.pack(side= LEFT,expand=YES, fill=BOTH)

bou5= Button(fen, text='PAUSE',  bg="ivory3", fg="white",font="Arial 20",bd=0,state =DISABLED)
bou5.pack(side= LEFT,expand=YES, fill=BOTH)

bou6= Button(fen, text='RECOMMENCER',  bg="ivory3", fg="white",font="Arial 20",bd=0,state =DISABLED,command=recom)
bou6.pack(side= LEFT,expand=YES, fill=BOTH)

fen.mainloop()



