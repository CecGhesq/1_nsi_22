import numpy as np
from sklearn.datasets import load_iris

def aa():
    base_iris = load_iris()
    donnees = base_iris.data
    selection_donnees = np.concatenate((donnees[0:7],donnees[50:57],donnees[100:107]))
    tableau = [0, 1, 2, 3, 4, 5, 6, 50, 51, 52, 53, 54, 55, 56, 100, 101, 102, 103, 104, 105, 106]
    etiquettes = base_iris.target
    donnees_iris = []
    nombre = 0
    for liste_donnees in selection_donnees:
        dico = {}
        dico['longueur_sepale'] = liste_donnees[0]
        dico['largeur_sepale'] = liste_donnees[1]
        dico['longueur_petale'] = liste_donnees[2]
        dico['largeur_petale'] = liste_donnees[3]
        if etiquettes[tableau[nombre]] == 0:
            dico['Nom']  = "Setosa"
        elif etiquettes[tableau[nombre]] == 1:
            dico['Nom']  = "Versicolor"
        elif etiquettes[tableau[nombre]] == 2:
            dico['Nom']  = "Virginica"
        donnees_iris.append(dico)
        nombre +=1
    return donnees_iris
            

