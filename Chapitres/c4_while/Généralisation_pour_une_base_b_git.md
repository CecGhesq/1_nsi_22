# Généralisation pour une base b

Nous avons vu ci-dessus comment convertir un nombre en base 10 en un nombre en base 16. Pour convertir un nombre n
(en base 10) en sa représentation en base b , il faut suivre l'algorithme suivant :

* On appelle D la liste des chiffres (le résultat).
* Tant que n>0
    * Faire la division euclidienne de n par b
    * Ajouter le reste à D 
    * Mettre le quotient dans n
* Inverser l'ordre de D

Soit **x** la représentation du nombre en base 10, **r** la représentation du nombre en base **b** , r étant écrit en utilisant n chiffres : c<sub>0</sub>, c<sub>1</sub>...c<sub>n-1</sub>, nous avons :

$`r = c_{n−1}  c_{n−2} … c_{2} c_{1} c_{0}`$  

$`x= \sum \limits_{\substack{i=0}}^{n-1}{c_i b^i} =c_{n-1} b^{n-1} + c_{n-2} b^{n-2}+ ..... +c_2 b^2 +c_1 b^1 +c_0 b^0`$ 
