---
title: "Chapitre 4 Boucles non bornées Tant que et  algorithme"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Algorithmes  

>📢 Un  __algorithme__ est  une  procédure  de  résolution  de  problème qui  produit, en un nombre fini d’étapes constructives, effectives, non-ambigües  et  organisées,  la  réponse  au  problème  pour  toute instance de ce dernier.

On souhaite écrire un entier naturel __n__ dans une base __b__.  
Voici un algorithme décrivant la suite d'étapes à réaliser.  

```
Entrées : b la base de numération, n un entier naturel.
Sortie : a0, a1,. . . les chiffres de l’écriture de n en base b.

1:  m ← n, i ← 0
2:  tant que m ≥ b faire
3:     r ← m (mod b)        // mod correspond à l'opérateur modulo
4:     m ← m ÷ b            // la division est entière car m est un entier
5:     ai ← chiffre correspondant à r
6:     i ← i + 1
7:  fin tant que
8:  ai ← chiffre correspondant à m
9:  renvoyer a0, a1, . . . , ai
```

> 📢L'algorithme présenté utilise du __pseudo-code__:  une manière de décrire un algorithme dans un langage presque naturel utilisant quelques conventions :
> 
> *  __Entrées__ : données néccessaires au fonctionnement de l'algorithme  
> * __Sortie__ : donnée fournie par l'algorithme    
> * __←__  symbolise l'_affectation_  
> * Indentation utilisée pour distinguer les blocs de code (conditions, boucles...)  
> * __//__  symbolise un commentaire     


On constate la présence d'une boucle de répétition __tant que__.  
Cela signigie que _"tant que la condition $`m \ge b`$ sera réalisée"_ toutes les instructions, présentes dans le bloc de code indenté, entre _"faire"_ et _"fin tant que"_ seront exécutées en boucle.  

Analysons le déroulement de cet algorithme avec un exemple :

* Entrée : __n=10__ et __b= 2__  

|                          | r   | m  |$`\bf a_{i}`$| i  |  
|:------------------------:|:---:|:--:|:--------:|:--:|  
|_etape 1 (initialisation)_|     |10  |          | 0  |  
|_boucle tour n°1 ($`10 \ge 2`$)_  |0    |5   |$`a_{0}=0`$| 1  |  
|_boucle tour n°2 ($`5 \ge 2`$)_   |1    |2   |$`a_{1}=1`$| 2  |  
|_boucle tour n°3 ($`2 \ge 2`$)_   |0    |1   |$`a_{2}=0`$| 3  |  
|_etape 8_                 |     |    |$`a_{3}=1`$|    |  

* Sortie : __0101__

La donnée fournie en sortie est $`a_{0}a_{1}a_{2}a_{2}`$, en remettant dans le bon ordre on a bien la représentation en binaire du nombre décimal 10 soit $`{(a_{3}a_{2}a_{1}a_{0})}_{2}`$ soit $`\bf {(1010)}_{2}`$


# 💾 La Boucle tant que en Python  

Voyons comment implémenter une boucle tant que en Python

```python
>>> x = 0
>>> while x < 3:
      x = x + 1
      print(x)
    print('Fin')
1
2
3
'Fin'
```

Le but de ce programme très simple est d'__afficher__ les nombres entiers allant de 1 à 3 en utilisant une fonction native de Python : `print`.    
_Note : nous reviendrons plus en détail sur cette fonction dans quelque temps_

Analysons le code :  
 
* Une variable `x` est initialisée avec la valeur `0`
* L'instruction `while(x < 3)` signifie *tant que x est inférieur à 3*.
* On retrouve juste après un bloc d'instructions indenté qui s'exécutera __tant que__ la condition `x < 3` est vérifiée autrement dit si l'expression correspondante est évaluée à `True`

On peut réaliser un tableau de suivi:    

|                          | x   | affichage  |   
|:------------------------:|:---:|:----------:|  
|_etape 1 (initialisation)_|0    |  |  
|_boucle tour n°1 (0<5)_   |1    |1   |  
|_boucle tour n°2 (1<5)_   |2    |2   |  
|_boucle tour n°3 (2<5)_   |3    |3   |  

La condition `x < 5` est ensuite vérifiée une dernière fois et est évaluée à : `False`.  
Le bloc d'instructions est alors ignoré et le programme se poursuit par l'affichage de la chaîne de caractères `'Fin'`

On peut retenir que la boucle while a la structure suivante :

```python
while <condition>:
    <instructions a exécuter si la condition est vraie>
```

# Terminaison d'une boucle 

L'arrêt des itérations d'une boucle tant que étant conditionné à l'échec d'un test : il est possible de rencontrer des cas où le programe ne s'arrête jamais. On parle alors de divergence (ou de non terminaison) ou en encore de _boucle infinie_.

_Par exemple_ : 

```python
i = 2
while i > 0 :
    print(i)
    i = i + 1
```

Parfois la situation est moins évidente, il faudra donc utiliser une outil pour prouver la terminaison :

> 📢 Un __variant de boucle__ est  une quantité entière qui :    
> 
> * doit être positive ou nulle pour rester dans la boucle ;
> * doit décroître strictement à chaque itération.
> 
> Un variant de boucle bien choisi permet de prouver qu'une boucle tant que se termine.  

Voici un algorithme de multiplication : 

```
Entrée : Deux nombres entiers naturels a et b
Sortie : le produit a x b

1:  x ← a,  y ← b,   z ← 0
2:  tant que y différent de 0 faire
3:     z ← z + x      
4:     y ← y - 1      
9:  renvoyer z
```

En effet multiplier a par b revient a réaliser une addition (a + a + a ...) b fois : c'est ce que réalise cet algorithme.

Quel est le variant ici ? 

* y est positif  à l'entrée de la boucle puisque _b_ doit être un entier naturel 
* Il est décréménté à chaque itération de 1 unité
Il deviendra nécessairement nul au bout de _b_ itérations.  

# Variantes de la boucle "Tant que"
## Continue  

> 📢 Le terme `continue` dans la boucle while en Python renvoie à la condition en l'entrée de la boucle. Toutes les actions dans le corps de boucle __ne sont pas réalisées__.  

![continue](./img/continue.png)


```python
var = 10
while var>0 : 
    var = var-1
    if var == 4 :
        continue
    print (var, end = " ")
```

    9 8 7 6 5 3 2 1 0 

## Break

> 📢 Le terme `break` __termine__ le corps boucle dans lequelle est le programme puis sort.  

![BREAK](./img/break.png)


```python
var = 10
while var>0 : 
    var = var-1
    if var == 4 :
        break
    print (var, end = " ")
```

    9 8 7 6 5 

Remarque :  **while 1**  

On trouve parfois dans certains codes, l'expression *while 1* : ceci peut s'exprimer aussi "while True". Dans ce cas la condition est toujours vraie , donc la boucle est infinie! pour sortir de la boucle, il faut ABSOLUMENT donc créer un `break` comme dans l'exemple ci-dessous. 
Cette instruction peut être bien utile parfois....


```python
while 1: # 1 est toujours vrai -> boucle infinie
    lettre = input("Tapez 'Q' pour quitter : ")
    if lettre == "Q":
        print("Fin de la boucle")
        break
```

    Tapez 'Q' pour quitter : A
    Tapez 'Q' pour quitter : E
    Tapez 'Q' pour quitter : Q
    Fin de la boucle
    

sources : 
* cours Patrice Thibaud ( Université de Lille)  
* Apprendre à programmer avec Python 3  Gérard Swinnen  
* https://www.programiz.com/python-programming/break-continue  
* https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/231260-apprenez-a-faire-des-boucles  
